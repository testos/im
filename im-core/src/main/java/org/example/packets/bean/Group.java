package org.example.packets.bean;

import cn.hutool.core.bean.BeanUtil;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.codecs.pojo.annotations.BsonId;
import org.bson.codecs.pojo.annotations.BsonIgnore;
import org.example.packets.LastMessage;
import org.example.packets.handler.message.ChatRespBody;
import org.example.packets.handler.user.GroupUser;

import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Group implements Serializable {

    /**
     * 群组ID
     */
    @BsonId
    private String roomId;

    /**
     * 排序
     */
    private long index;

    /**
     * 是否好友分组
     */
    private Boolean isFriend;

    /**
     * 群组名称
     */
    private String roomName;

    /**
     * 头像
     */
    private String avatar;

    /**
     * 是否系统会话
     */
    private Boolean isSystem;

    /**
     * 是系统会话是， 系统会话的接收人是谁
     */
    private String userId;

    /**
     * 好友ID
     */
    @BsonIgnore
    private String friendId;

    /**
     * 最后一条消息
     */
    private LastMessage lastMessage;

    /**
     * 是否删除
     */
    private Boolean isDeleted;

    /**
     * 是否打开通知
     */
    private Boolean notice;

    /**
     * 是否拉黑
     */
    private Boolean black;

    /**
     * 组用户
     */
    private List<GroupUser> users;

    /**
     * 是否公开群组
     */
    private Boolean publicRoom;

    /**
     * 未读消息数量
     */
    @BsonIgnore
    private Integer unreadCount;

    /**
     * 群组公告编号
     */
    private String announcementId;

    /**
     * 群组公告内容
     */
    @BsonIgnore
    private ChatRespBody announcement;

    public Group clone(){
        return BeanUtil.copyProperties(this,Group.class,"users");
    }

}

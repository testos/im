package org.example.packets.bean;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.codecs.pojo.annotations.BsonId;
import org.example.packets.Status;

import java.io.Serializable;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User implements Serializable {

    /**
     * 主键
     */
    @BsonId
    @JSONField(name = "_id")
    private String id;

    /**
     * 用户名/ 昵称
     */
    private String username;

    /**
     * 账户
     */
    private String account;

    /**
     * 头像
     */
    private String avatar;

    /**
     * 用户状态
     */
    private Status status;

    /**
     * 是否系统账号
     */
    private Boolean isSystem;

}

package org.example.packets.handler.user;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import lombok.experimental.Accessors;
import org.example.enums.RoomRoleEnum;
import org.example.packets.Status;

@Data
@Accessors(chain = true)
public class GroupUser {

    /**
     * 主键
     */
    @JSONField(name = "_id")
    private String id;

    /**
     * 用户名/ 昵称
     */
    private String username;

    /**
     * 账户
     */
    private String account;

    /**
     * 头像
     */
    private String avatar;

    /**
     * 用户状态
     */
    private Status status;

    /**
     * 是否系统账号
     */
    private Boolean isSystem;

    /**
     * 是否拉黑
     */
    private Boolean black;

    /**
     * 群组身份
     */
    private RoomRoleEnum role;


}

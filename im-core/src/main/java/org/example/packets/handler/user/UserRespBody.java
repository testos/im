package org.example.packets.handler.user;

import lombok.Data;
import lombok.experimental.Accessors;
import org.example.packets.bean.Group;
import org.example.packets.bean.User;

import java.util.List;

@Accessors(chain = true)
@Data
public class UserRespBody {

    private User user;

    private List<Group> groups;
}

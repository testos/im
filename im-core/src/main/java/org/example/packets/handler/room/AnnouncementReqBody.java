package org.example.packets.handler.room;

import lombok.Data;

@Data
public class AnnouncementReqBody {

    /**
     * 消息ID
     */
    private String _id;

    /**
     * 房间号
     */
    private String roomId;

    /**
     * 内容
     */
    private String content;
}

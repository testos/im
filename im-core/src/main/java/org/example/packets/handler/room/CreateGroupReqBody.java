package org.example.packets.handler.room;

import lombok.Data;
import org.example.packets.bean.User;
import org.example.packets.handler.user.GroupUser;

import java.util.List;

@Data
public class CreateGroupReqBody {

    /**
     * 群组名称
     */
    private String roomName;

    /**
     * 是否好友会话
     */
    private Boolean isFriend;

    /**
     * 头像 (群组设置)
     */
    private String avatar;

    /**
     * 群组创建时携带的人
     */
    private List<GroupUser> users;

    /**
     * 是否公开群组
     */
    private Boolean publicRoom;
}

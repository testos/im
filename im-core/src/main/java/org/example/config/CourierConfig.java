package org.example.config;

import cn.hutool.core.util.StrUtil;
import cn.hutool.setting.Setting;
import lombok.Data;

import java.nio.charset.Charset;

@Data
public class CourierConfig {

    /**
     * 应用名称
     */
    private String applicationName;

    /**
     * Socket端口
     */
    private Integer socketPort;

    /**
     * 心跳间隔
     */
    private Integer socketHeartbeat;

    /**
     * Http端口
     */
    private Integer httpPort;

    /**
     * 最大保持
     */
    private Integer httpMaxLiveTime;

    /**
     * 是否使用session
     */
    private Boolean httpUseSession;

    /**
     * 检查Host
     */
    private Boolean httpCheckHost;

    /**
     * 检查Md5
     */
    private Boolean checkFileMd5;

    /**
     * 数据库端口
     */
    private String mongoHost;

    /**
     * 数据库用户名
     */
    private String mongoUserName;

    /**
     * 数据库密码
     */
    private String mongoPassword;

    /**
     * 文件地址
     */
    private String fileUrl;

    /**
     * 文件存储地址
     */
    private String minioHost;

    /**
     * 文件存储端口
     */
    private Integer minioPort;

    /**
     * 文件存储用户名
     */
    private String minioAccessKey;

    /**
     * 文件存储密码
     */
    private String minioSecretKey;

    /**
     * 文件存储是否使用SSL
     */
    private Boolean minioUseSSL;

    private String env;

    public static CourierConfig get() {
        return Global.get();
    }

    public static class Global {
        private static CourierConfig config;

        public static CourierConfig get() {
            return config;
        }

        public static void set(CourierConfig imConfig) {
            CourierConfig.Global.config = imConfig;
        }
    }

    static {
        Setting setting = new Setting("application.setting", Charset.defaultCharset(), true);

        String property = System.getProperty("ENV");
        String env = StrUtil.isNotBlank(property) ? property : "dev";

        CourierConfig courierConfig = setting.toBean(env, CourierConfig.class);
        setting.toBean(courierConfig);
        courierConfig.setEnv(env);
        Global.set(courierConfig);
    }
}

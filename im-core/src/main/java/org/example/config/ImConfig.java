package org.example.config;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.example.listener.ImGroupListener;
import org.example.listener.ImGroupPropListener;
import org.example.listener.ImUserListener;
import org.example.listener.ImUserPropListener;
import org.tio.core.TioConfig;
import org.tio.core.intf.GroupListener;
import org.tio.utils.prop.MapWithLockPropSupport;
import org.tio.utils.time.Time;

@EqualsAndHashCode(callSuper = true)
@Data
public class ImConfig extends MapWithLockPropSupport {

    /**
     * 协议名字(可以随便取，主要用于开发人员辨识)
     */
    public static final String PROTOCOL_NAME = "IM";

    public static final String CHARSET = "utf-8";

    /**
     * 监听的ip
     */
    public static final String SERVER_IP = null; // null表示监听所有，并不指定ip

    public TioConfig tioConfig;

    /**
     * 用户绑定监听器
     */
    public ImUserListener imUserListener;

    /**
     * 用户信息变化监听器
     */
    public ImUserPropListener imUserPropListener;

    /**
     * 群组信息变化监听器
     */
    public ImGroupPropListener imGroupPropListener;

    /**
     * ip数据监控统计，时间段
     *
     * @author tanyaowu
     */
    public interface IpStatDuration {
        Long DURATION_1 = Time.MINUTE_1 * 5;
        Long[] IP_STAT_DURATIONS = new Long[]{DURATION_1};
    }

    ImConfig() {
        ImConfig.Global.set(this);
    }

    public static ImConfig get() {
        return ImConfig.Global.get();
    }

    public static ImUserListener userListener() {
        return ImConfig.Global.get().getImUserListener();
    }

    public static ImGroupPropListener groupPropListener() {
        return ImConfig.Global.get().getImGroupPropListener();
    }

    public static ImGroupListener groupListener() {
        return ((ImGroupListener) Global.get().getTioConfig().getGroupListener());
    }

    public static class Global {
        private static ImConfig imConfig;

        public static ImConfig get() {
            return imConfig;
        }

        public static void set(ImConfig imConfig) {
            Global.imConfig = imConfig;
        }
    }
}

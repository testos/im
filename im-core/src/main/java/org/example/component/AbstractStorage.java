package org.example.component;

/**
 * AbstractStorage
 * <p>
 * 用于存储数据于当前线程中
 *
 * @author ch
 */
public class AbstractStorage {
    /**
     * 存储空间
     */
    protected static final ThreadLocal<LoginInfo> THREAD_LOCAL = new ThreadLocal<>();

    /**
     * 存储登陆者信息
     * <p>
     * 会删除上一次存储的信息 如果不为空
     *
     * @param value 登陆者信息
     */
    public static void setValue(LoginInfo value) {
        LoginInfo loginInfo = THREAD_LOCAL.get();
        if (loginInfo != null) {
            THREAD_LOCAL.remove();
        }
        THREAD_LOCAL.set(value);
    }

    /**
     * 清空数据
     * <p>
     */
    public static void clear() {
        THREAD_LOCAL.remove();
    }

}

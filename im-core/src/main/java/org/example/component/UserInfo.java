package org.example.component;

import cn.hutool.core.collection.CollUtil;
import org.example.packets.bean.Group;
import org.example.packets.bean.User;

import java.util.ArrayList;
import java.util.List;

public class UserInfo extends AbstractStorage {

    public static LoginInfo get() {
        return THREAD_LOCAL.get();
    }

    public static String id() {
        LoginInfo loginInfo = get();
        return loginInfo.getUser().getId();
    }

    public static User user() {
        LoginInfo loginInfo = get();
        return loginInfo.getUser();
    }

    public static List<Group> groups() {
        LoginInfo loginInfo = get();
        return CollUtil.defaultIfEmpty(loginInfo.getGroups(), new ArrayList<>());
    }

    public static Group group(String roomId) {
        LoginInfo loginInfo = get();
        return loginInfo.getGroupsMap().get(roomId);
    }

    public static Group friend(String friendId) {
        LoginInfo loginInfo = get();
        return loginInfo.getFriendGroupMap().get(friendId);
    }

    public static void putGroup(Group group) {
        LoginInfo loginInfo = get();
        loginInfo.putGroup(group);
    }
}

package org.example.listener;

import org.example.packets.bean.Group;

public interface ImGroupPropListener {

    void onPropChange(Group group);

    void onReadMessage( String roomId);
}

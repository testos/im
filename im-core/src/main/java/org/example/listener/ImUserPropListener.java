package org.example.listener;

import org.example.packets.Status;

public interface ImUserPropListener {

    void onStatusChange(Status status);

}

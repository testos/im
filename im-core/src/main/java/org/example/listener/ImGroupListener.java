package org.example.listener;

import org.example.enums.RoomRoleEnum;
import org.tio.core.intf.GroupListener;

public interface ImGroupListener extends GroupListener {

    void onRemoveUser(String roomId, String userId);

    void onDisband(String roomId);

    void onRoleChange(String roomId, String userId, RoomRoleEnum role);

    void onBlack(String roomId, String userId, Boolean black);

    void onMoveTop(String roomId, String id, Long index);
}

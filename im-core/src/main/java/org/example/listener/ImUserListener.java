package org.example.listener;

import org.example.packets.bean.User;
import org.tio.core.ChannelContext;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public interface ImUserListener {

    Map<String, User> users = new ConcurrentHashMap<>();

    void onAfterBind(ChannelContext channelContext, User user);

    void onAfterUnbind(ChannelContext channelContext, User user);

    void onAfterChange(User user);

    User onGetUser(String userId);

}

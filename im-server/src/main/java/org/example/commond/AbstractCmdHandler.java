package org.example.commond;

import org.example.service.*;

public abstract class AbstractCmdHandler implements CmdHandler {

    public UserService userService;
    public GroupService groupService;
    public UserGroupService userGroupService;
    public MessageService messageService;
    public UnReadMessageService unReadMessageService;
    public AuthService authService;
    public EmoticonService emoticonService;


    public AbstractCmdHandler() {
        userService = new UserService();
        groupService = new GroupService();
        userGroupService = new UserGroupService();
        messageService = new MessageService();
        unReadMessageService = new UnReadMessageService();
        authService = new AuthService();
        emoticonService = new EmoticonService();
    }

}

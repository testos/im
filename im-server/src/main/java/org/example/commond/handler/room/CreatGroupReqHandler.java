package org.example.commond.handler.room;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import lombok.extern.slf4j.Slf4j;
import org.example.commond.AbstractCmdHandler;
import org.example.commond.CommandManager;
import org.example.component.UserInfo;
import org.example.config.Im;
import org.example.enums.CommandEnum;
import org.example.enums.DefaultEnum;
import org.example.enums.JoinGroupEnum;
import org.example.enums.RoomRoleEnum;
import org.example.packets.bean.Group;
import org.example.packets.bean.User;
import org.example.packets.handler.room.CreateGroupReqBody;
import org.example.packets.handler.room.JoinGroupNotifyBody;
import org.example.packets.handler.system.RespBody;
import org.example.packets.handler.user.GroupUser;
import org.example.protocol.http.service.UploadService;
import org.example.util.JsonUtil;
import org.tio.core.ChannelContext;
import org.tio.core.intf.Packet;
import org.tio.websocket.common.WsRequest;
import org.tio.websocket.common.WsResponse;

import java.util.ArrayList;

@Slf4j
public class CreatGroupReqHandler extends AbstractCmdHandler {

    @Override
    public CommandEnum command() {
        return CommandEnum.COMMAND_CREATE_GROUP_REQ;
    }

    @Override
    public WsResponse handler(Packet packet, ChannelContext channelContext) {
        log.info("创建群组");
        CreateGroupReqBody request = JsonUtil.format(packet, CreateGroupReqBody.class);
        User imUser = UserInfo.user();
        GroupUser user = BeanUtil.copyProperties(imUser, GroupUser.class);
        // 创建群聊
        Group group = Group.builder().roomId(IdUtil.getSnowflake().nextIdStr())
                .isFriend(request.getIsFriend())
                .index(System.currentTimeMillis())
                .roomName(request.getRoomName())
                .publicRoom(request.getPublicRoom())
                .isSystem(false)
                .users(new ArrayList<>())
                .build();

        if (!request.getIsFriend()) {
            String url = UploadService.uploadDefault(DefaultEnum.ACCOUNT_GROUP);
            user.setRole(RoomRoleEnum.ADMIN);

            group.setAvatar(StrUtil.isBlank(request.getAvatar()) ? url : request.getAvatar());

        } else {
            user.setRole(RoomRoleEnum.CREATE);
        }

        // 处理用户信息
        request.getUsers().add(user);

        groupService.saveOrUpdateById(group);

        WsResponse wsResponse = WsResponse.fromText(RespBody.success(CommandEnum.COMMAND_CREATE_GROUP_RESP, group), Im.CHARSET);
        Im.send(channelContext, wsResponse);


        JoinGroupNotifyBody joinGroupNotifyBody = JoinGroupNotifyBody.builder().group(group).users(request.getUsers())
                .code(JoinGroupEnum.STATE_CREATE.getValue()).build();

        // 发送加入群聊消息
        WsRequest wsRequest = WsRequest.fromText(JSON.toJSONString(joinGroupNotifyBody, SerializerFeature.DisableCircularReferenceDetect), Im.CHARSET);
        JoinGroupReqHandler command = CommandManager.getCommand(CommandEnum.COMMAND_JOIN_GROUP_REQ, JoinGroupReqHandler.class);
        command.handler(wsRequest, channelContext);

        return null;
    }
}

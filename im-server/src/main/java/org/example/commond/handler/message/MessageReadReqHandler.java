package org.example.commond.handler.message;

import com.alibaba.fastjson.JSONObject;
import org.example.commond.AbstractCmdHandler;
import org.example.component.UserInfo;
import org.example.config.Im;
import org.example.config.ImConfig;
import org.example.enums.CommandEnum;
import org.example.packets.bean.Group;
import org.example.packets.bean.Message;
import org.example.packets.bean.UnReadMessage;
import org.example.packets.handler.message.MessageReadReqBody;
import org.example.packets.handler.system.RespBody;
import org.example.util.ChannelContextUtil;
import org.example.util.JsonUtil;
import org.tio.core.ChannelContext;
import org.tio.core.intf.Packet;
import org.tio.utils.hutool.CollUtil;
import org.tio.websocket.common.WsRequest;
import org.tio.websocket.common.WsResponse;

import java.util.List;

public class MessageReadReqHandler extends AbstractCmdHandler {

    @Override
    public CommandEnum command() {
        return CommandEnum.COMMAND_MESSAGE_READ_REQ;
    }

    @Override
    public WsResponse handler(Packet packet, ChannelContext channelContext) {

        MessageReadReqBody messageReadReqBody = JsonUtil.format(packet, MessageReadReqBody.class);

        // 获取当前房间所有的未读消息
        List<UnReadMessage> unReadMessage = unReadMessageService.getUnReadMessage(UserInfo.user().getId(), messageReadReqBody.getRoomId());

        unReadMessageService.clearUnReadMessage(UserInfo.user().getId(), messageReadReqBody.getRoomId());

        // 设置相关连接未读消息数量为0
        ImConfig.groupPropListener().onReadMessage(messageReadReqBody.getRoomId());

        Group groupInfo = groupService.getGroupInfo(messageReadReqBody.getRoomId());

        // 检查这个消息是不是全部已读
        for (UnReadMessage readMessage : unReadMessage) {
            List<UnReadMessage> messageUnReads = unReadMessageService.getMessageUnReads(readMessage.getMessageId());
            // 如果为空 全部已读
            if (CollUtil.isEmpty(messageUnReads)) {
                messageReadReqBody.setMessageId(readMessage.getMessageId());

                // 更新消息为已读状态
                messageService.read(readMessage.getMessageId());

                // 如果当前群组消息等于最后一条消息
                if (groupInfo.getLastMessage().getMessageId().equals(readMessage.getMessageId())) {
                    groupService.readLastMessage(groupInfo);
                }

                Message message = messageService.getMessage(readMessage.getMessageId());
                // 发送给消息发送者
                WsResponse response = WsResponse.fromText(RespBody.success(CommandEnum.COMMAND_MESSAGE_READ_RESP, messageReadReqBody), Im.CHARSET);
                List<ChannelContext> channelContextList = ChannelContextUtil.contextList(message.getSenderId());
                for (ChannelContext context : channelContextList) {
                    Im.send(context, response);
                }
            }
        }


        return null;
    }
}

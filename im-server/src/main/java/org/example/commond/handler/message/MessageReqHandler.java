package org.example.commond.handler.message;

import cn.hutool.core.bean.BeanUtil;
import org.example.commond.AbstractCmdHandler;
import org.example.component.UserInfo;
import org.example.config.Im;
import org.example.config.ImConfig;
import org.example.enums.CommandEnum;
import org.example.enums.MessageFetchTypeEnum;
import org.example.packets.bean.Message;
import org.example.packets.bean.User;
import org.example.packets.handler.message.ChatRespBody;
import org.example.packets.handler.message.MessageReqBody;
import org.example.packets.handler.message.MessageRespBody;
import org.example.packets.handler.system.RespBody;
import org.example.util.JsonUtil;
import org.tio.core.ChannelContext;
import org.tio.core.intf.Packet;
import org.tio.websocket.common.WsResponse;

import java.util.List;
import java.util.stream.Collectors;

public class MessageReqHandler extends AbstractCmdHandler {

    @Override
    public CommandEnum command() {
        return CommandEnum.COMMAND_GET_MESSAGE_REQ;
    }

    @Override
    public WsResponse handler(Packet packet, ChannelContext channelContext) {

        MessageReqBody messageReqBody = JsonUtil.format(packet,MessageReqBody.class);
        if (messageReqBody.getType() == null) {
            messageReqBody.setType(MessageFetchTypeEnum.TOP);
        }
        User user = UserInfo.user();
        List<Message> messages = messageService.getHistoryMessage(messageReqBody.getRoomId(), messageReqBody.getMessageId(), messageReqBody.getType(), user.getId());

        List<ChatRespBody> collect = messages.stream().map(x -> {
            ChatRespBody chatRespBody = BeanUtil.copyProperties(x, ChatRespBody.class);
            User userInfo = ImConfig.userListener().onGetUser(chatRespBody.getSenderId());
            chatRespBody.setAvatar(userInfo.getAvatar());
            chatRespBody.setUsername(userInfo.getUsername());
            chatRespBody.setCurrentUserId(user.getId());
            return chatRespBody;
        }).collect(Collectors.toList());

        MessageRespBody messageRespBody = new MessageRespBody();
        messageRespBody.setRoomId(messageReqBody.getRoomId());
        messageRespBody.setMessageId(messageReqBody.getMessageId());
        messageRespBody.setMessages(collect);
        messageRespBody.setType(messageReqBody.getType());
        messageRespBody.setReturnDefault(messageReqBody.getReturnDefault());
        messageRespBody.setGroupAnnouncementRead(groupService.announcementRead(user.getId(), messageReqBody.getRoomId()));

        String success = RespBody.success(CommandEnum.COMMAND_GET_MESSAGE_RESP, messageRespBody);
        WsResponse response = WsResponse.fromText(success, Im.CHARSET);
        Im.send(channelContext, response);

        return null;
    }

}

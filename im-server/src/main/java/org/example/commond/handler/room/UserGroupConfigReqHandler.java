package org.example.commond.handler.room;

import org.example.commond.AbstractCmdHandler;
import org.example.component.UserInfo;
import org.example.config.Im;
import org.example.config.ImConfig;
import org.example.enums.CommandEnum;
import org.example.packets.bean.Group;
import org.example.packets.bean.User;
import org.example.packets.bean.UserGroup;
import org.example.packets.handler.room.UserGroupConfigReqBody;
import org.example.packets.handler.system.RespBody;
import org.example.util.ChannelContextUtil;
import org.example.util.JsonUtil;
import org.tio.core.ChannelContext;
import org.tio.core.intf.Packet;
import org.tio.websocket.common.WsResponse;

import java.util.List;

/**
 * 群组用户修改自己的配置
 * <p>
 * 通知 / 群组备注 / 成员备注 等
 * </p>
 *
 * @author smart
 * @since 1.0.0
 */
public class UserGroupConfigReqHandler extends AbstractCmdHandler {

    @Override
    public CommandEnum command() {
        return CommandEnum.COMMAND_USER_GROUP_CONFIG_REQ;
    }

    @Override
    public WsResponse handler(Packet packet, ChannelContext channelContext) {

        UserGroupConfigReqBody body = JsonUtil.format(packet, UserGroupConfigReqBody.class);

        switch (body.getType()) {
            case NOTICE:
                setNotice(body, channelContext);
                break;
            case GROUP_REMARK:
                setGroupRemark(body, channelContext);
                break;
            case MOVE_TOP:
                setMoveTop(body);
                break;
            case BLACK:
                setBlack(body);
                break;
            default:
                break;
        }

        return null;
    }

    private void setBlack(UserGroupConfigReqBody config) {
        User user = UserInfo.user();
        UserGroup userGroup = userGroupService.getUserGroup(config.getRoomId(), user.getId());
        userGroup.setBlack(config.getBlack());
        userGroupService.update(userGroup);

        ImConfig.groupListener().onBlack(config.getRoomId(), UserInfo.id(), config.getBlack());

        String success = RespBody.success(CommandEnum.COMMAND_USER_GROUP_CONFIG_RESP, config);
        WsResponse response = WsResponse.fromText(success, Im.CHARSET);

        Im.sendToGroup(config.getRoomId(),response);

    }

    private void setMoveTop(UserGroupConfigReqBody config) {
        User user = UserInfo.user();
        UserGroup userGroup = userGroupService.getUserGroup(config.getRoomId(), user.getId());
        userGroup.setTop(config.getMoveTop());
        userGroupService.update(userGroup);

        if (Boolean.TRUE.equals(config.getMoveTop())) {
            config.setIndex(9999999999999L);
        } else {
            Group group = groupService.getGroupInfo(config.getRoomId());
            config.setIndex(group.getIndex());
        }

        ImConfig.groupListener().onMoveTop(config.getRoomId(), UserInfo.id(), config.getIndex());

        String success = RespBody.success(CommandEnum.COMMAND_USER_GROUP_CONFIG_RESP, config);
        WsResponse response = WsResponse.fromText(success, Im.CHARSET);

        List<ChannelContext> channelContexts = ChannelContextUtil.contextList(UserInfo.id());
        for (ChannelContext context : channelContexts) {
            Im.send(context, response);
        }

    }

    /**
     * 设置通知
     *
     * @param config         配置信息
     * @param channelContext 上下文信息
     */
    private void setNotice(UserGroupConfigReqBody config, ChannelContext channelContext) {

        User user = UserInfo.user();
        Group contextGroup = UserInfo.group(config.getRoomId());
        UserGroup userGroup = userGroupService.getUserGroup(config.getRoomId(), user.getId());
        userGroup.setNotice(config.getNotice());
        userGroupService.update(userGroup);
        contextGroup.setNotice(config.getNotice());

        // TODO GROUP prop change

        WsResponse wsResponse = WsResponse.fromText(RespBody.success(CommandEnum.COMMAND_USER_GROUP_CONFIG_RESP, config), Im.CHARSET);
        Im.send(channelContext, wsResponse);
    }

    /**
     * 设置群组备注
     *
     * @param config         配置信息
     * @param channelContext 上下文信息
     */
    private void setGroupRemark(UserGroupConfigReqBody config, ChannelContext channelContext) {

    }
}

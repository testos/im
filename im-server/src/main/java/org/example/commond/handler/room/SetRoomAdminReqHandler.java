package org.example.commond.handler.room;

import cn.hutool.core.bean.BeanUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import org.example.commond.AbstractCmdHandler;
import org.example.commond.CommandManager;
import org.example.component.UserInfo;
import org.example.config.Chat;
import org.example.config.Im;
import org.example.config.ImConfig;
import org.example.enums.CommandEnum;
import org.example.enums.GroupAdminTypeEnum;
import org.example.enums.RoomRoleEnum;
import org.example.packets.bean.User;
import org.example.packets.bean.UserGroup;
import org.example.packets.handler.message.ChatReqBody;
import org.example.packets.handler.user.GroupUser;
import org.example.packets.handler.user.UserStatusBody;
import org.example.packets.handler.room.GroupAdminReqBody;
import org.example.util.JsonUtil;
import org.tio.core.ChannelContext;
import org.tio.core.intf.Packet;
import org.tio.websocket.common.WsRequest;
import org.tio.websocket.common.WsResponse;

public class SetRoomAdminReqHandler extends AbstractCmdHandler {

    @Override
    public CommandEnum command() {
        return CommandEnum.COMMAND_SET_ROOM_ADMIN_REQ;
    }

    @Override
    public WsResponse handler(Packet packet, ChannelContext channelContext) {

        GroupAdminReqBody body = JsonUtil.format(packet,GroupAdminReqBody.class);

        User user = UserInfo.user();

        UserGroup userGroup = userGroupService.getUserGroup(body.getRoomId(), body.getUserId());
        if (userGroup == null) {
            return null;
        }

        userGroup.setRole(body.getType().equals(GroupAdminTypeEnum.SET) ? RoomRoleEnum.SUB_ADMIN : RoomRoleEnum.GENERAL);
        userGroupService.update(userGroup);

        User userInfo = ImConfig.userListener().onGetUser(body.getUserId());
        GroupUser groupUser = BeanUtil.copyProperties(userInfo, GroupUser.class);
        groupUser.setRole(userGroup.getRole());

        // 用户状态变化消息
        UserStatusBody userStatusBody = new UserStatusBody();
        userStatusBody.setRoomId(body.getRoomId());
        userStatusBody.setUser(groupUser);

        Chat.sendToGroup(userStatusBody);

        ImConfig.groupListener().onRoleChange(body.getRoomId(), body.getUserId(),userGroup.getRole());

        // 移交群主消息
        AbstractCmdHandler command = CommandManager.getCommand(CommandEnum.COMMAND_CHAT_REQ);
        String content;
        if (body.getType().equals(GroupAdminTypeEnum.SET)) {
            // 发送退出群聊消息
            content = user.getUsername() + "已设置\"" + userService.getUserInfo(body.getUserId()).getUsername() + "\"为管理员";
        } else {
            content = user.getUsername() + "已解除\"" + userService.getUserInfo(body.getUserId()).getUsername() + "\"管理员身份";
        }

        ChatReqBody chatReqBody = ChatReqBody.buildSystem(body.getRoomId(), user.getId(), content);

        WsRequest wsRequest = WsRequest.fromText(JSON.toJSONString(chatReqBody, SerializerFeature.DisableCircularReferenceDetect), Im.CHARSET);

        command.handler(wsRequest, channelContext);
        return null;
    }

}

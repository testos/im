package org.example.commond.handler.emoticon;

import cn.hutool.core.io.file.FileNameUtil;
import cn.hutool.core.util.CharUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import org.example.commond.AbstractCmdHandler;
import org.example.component.UserInfo;
import org.example.config.Im;
import org.example.enums.CommandEnum;
import org.example.packets.bean.Emoticon;
import org.example.packets.bean.User;
import org.example.packets.handler.emoticon.EmoticonOperationReqBody;
import org.example.packets.handler.emoticon.EmoticonOperationRespBody;
import org.example.packets.handler.system.RespBody;
import org.example.service.UserEmoticonService;
import org.example.util.JsonUtil;
import org.tio.core.ChannelContext;
import org.tio.core.intf.Packet;
import org.tio.websocket.common.WsResponse;

public class EmoticonOperationReqHandler extends AbstractCmdHandler {

    private final UserEmoticonService userEmoticonService;

    public EmoticonOperationReqHandler() {
        userEmoticonService = new UserEmoticonService();
    }

    @Override
    public CommandEnum command() {
        return CommandEnum.COMMAND_EMOTICON_OPERATION_REQ;
    }

    @Override
    public WsResponse handler(Packet packet, ChannelContext channelContext) {

        User user = UserInfo.user();

        EmoticonOperationReqBody reqBody = JsonUtil.format(packet, EmoticonOperationReqBody.class);
        EmoticonOperationRespBody respBody = new EmoticonOperationRespBody();
        respBody.setType(reqBody.getType());

        switch (reqBody.getType()) {
            case INSERT_TO_USER:
                respBody.setEmoticon(this.insertToUser(reqBody, user.getId()));
                break;
            case DELETE:
                respBody.setEmoticon(this.deleteUserEmoticon(reqBody, user.getId()));
                break;
            case INSERT_TO_STORE:
                respBody.setEmoticon(this.insertToStore(reqBody));
                break;
            case INSERT_TO_USER_AND_STORE:
                this.insertToUserAndStore(reqBody, user.getId());
                break;
            case INSERT_EMOTICON_TO_USER:
                respBody.setEmoticon(this.insertEmoticonToUser(reqBody, user.getId()));
                break;
            case MOVE_TOP:
                respBody.setEmoticon(this.moveTop(reqBody));
                break;
        }

        String success = RespBody.success(CommandEnum.COMMAND_EMOTICON_OPERATION_RESP, respBody);
        WsResponse response = WsResponse.fromText(success, Im.CHARSET);
        Im.send(channelContext, response);
        return null;
    }

    private Emoticon moveTop(EmoticonOperationReqBody reqBody) {
        Emoticon emoticon = emoticonService.getEmoticon(reqBody.getEmoticonId());
        emoticon.setIndex(System.currentTimeMillis());
        emoticonService.update(emoticon);
        return emoticon;
    }

    private Emoticon insertEmoticonToUser(EmoticonOperationReqBody reqBody, String userId) {
        userEmoticonService.insert(reqBody.getEmoticonId(), userId);
        return emoticonService.getEmoticon(reqBody.getEmoticonId());
    }

    private void insertToUserAndStore(EmoticonOperationReqBody body, String userId) {
        String suffix = FileNameUtil.getSuffix(body.getUrl());
        String name = body.getName() + (StrUtil.isNotBlank(suffix) ? CharUtil.DOT : "") + suffix;
        String id = emoticonService.insert(name, body.getSize(), body.getUrl(), false);

        userEmoticonService.insert(id, userId);
    }

    private Emoticon insertToStore(EmoticonOperationReqBody reqBody) {
        Emoticon emoticon = emoticonService.getEmoticon(reqBody.getEmoticonId());
        emoticon.setIndex(System.currentTimeMillis());
        emoticon.setIsPrivate(false);
        emoticonService.update(emoticon);
        return emoticon;
    }

    private Emoticon insertToUser(EmoticonOperationReqBody body, String userId) {
        String suffix = FileNameUtil.getSuffix(body.getUrl());
        String name = IdUtil.getSnowflakeNextIdStr() + (StrUtil.isNotBlank(suffix) ? CharUtil.DOT : "") + suffix;
        String id = emoticonService.insert(name, body.getSize(), body.getUrl(), true);

        userEmoticonService.insert(id, userId);
        return emoticonService.getEmoticon(id);
    }

    private Emoticon deleteUserEmoticon(EmoticonOperationReqBody body,  String userId) {
        userEmoticonService.delete(body.getEmoticonId(), userId);
        return emoticonService.getEmoticon(body.getEmoticonId());
    }
}

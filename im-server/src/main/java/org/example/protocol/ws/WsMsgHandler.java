package org.example.protocol.ws;

import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSON;
import org.example.commond.AbstractCmdHandler;
import org.example.commond.CommandManager;
import org.example.commond.handler.LoginReqHandler;
import org.example.component.LoginInfo;
import org.example.component.UserInfo;
import org.example.config.Im;
import org.example.config.ImConfig;
import org.example.enums.CommandEnum;
import org.example.packets.bean.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tio.core.ChannelContext;
import org.tio.http.common.HttpRequest;
import org.tio.http.common.HttpResponse;
import org.tio.websocket.common.WsRequest;
import org.tio.websocket.common.WsResponse;
import org.tio.websocket.server.handler.IWsMsgHandler;

import static org.example.component.AbstractStorage.clear;
import static org.example.component.AbstractStorage.setValue;

public class WsMsgHandler implements IWsMsgHandler {
    private static final Logger log = LoggerFactory.getLogger(WsMsgHandler.class);

    public static final WsMsgHandler me = new WsMsgHandler();


    @Override
    public HttpResponse handshake(HttpRequest httpRequest, HttpResponse httpResponse, ChannelContext channelContext) {
        String clientIp = httpRequest.getClientIp();
        log.info("收到来自{}的ws握手包", clientIp);
        System.out.println("handshake");
        return httpResponse;
    }

    @Override
    public void onAfterHandshaked(HttpRequest httpRequest, HttpResponse httpResponse, ChannelContext channelContext) {
//        // 设置线程上下文
//        setValue(new LoginInfo().setChannelContext(channelContext));
        System.out.println("onAfterHandshaked");
        LoginReqHandler loginHandler = (LoginReqHandler) CommandManager.getCommand(CommandEnum.COMMAND_LOGIN_REQ);

        String token = httpRequest.getParam("token");
        WsRequest wsRequest = WsRequest.fromText(token, ImConfig.CHARSET);
        loginHandler.handler(wsRequest, channelContext);
        log.info("握手完毕{}", token);

        clear();
    }

    @Override
    public Object onBytes(WsRequest wsRequest, byte[] bytes, ChannelContext channelContext) {
        return null;
    }

    @Override
    public Object onClose(WsRequest wsRequest, byte[] bytes, ChannelContext channelContext) {
        System.out.println("onClose");
        AbstractCmdHandler command = CommandManager.getCommand(CommandEnum.COMMAND_CLOSE_REQ);
        User user = UserInfo.user();
        if (user != null) {
            WsRequest request = WsRequest.fromText(user.getId(), Im.CHARSET);
            command.handler(request, channelContext);
            System.out.println("关闭连接");
        }
        return null;
    }

    @Override
    public Object onText(WsRequest wsRequest, String text, ChannelContext channelContext) {
        Integer cmd = JSON.parseObject(text).getInteger("cmd");
        if (cmd != 13) {
//            log.info("socket消息:{}", text);
        }
        CommandEnum commandEnum = CommandEnum.forNumber(cmd);
        AbstractCmdHandler command = CommandManager.getCommand(commandEnum);
        WsResponse wsResponse = command.handler(wsRequest, channelContext);
        if (ObjectUtil.isNotNull(wsResponse)) {
            Im.send(channelContext, wsResponse);
        }

        clear();
        return null;
    }
}

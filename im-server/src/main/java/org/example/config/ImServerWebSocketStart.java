package org.example.config;

import org.example.listener.*;
import org.example.protocol.ws.WsMsgHandler;
import org.tio.server.TioServerConfig;
import org.tio.websocket.server.WsServerStarter;

import java.io.IOException;

public class ImServerWebSocketStart {

    private final WsServerStarter wsServerStarter;
    private final TioServerConfig serverTioConfig;


    public ImServerWebSocketStart(int port, WsMsgHandler wsMsgHandler) throws IOException {
        wsServerStarter = new WsServerStarter(port, wsMsgHandler);

        serverTioConfig = wsServerStarter.getTioServerConfig();
        serverTioConfig.setGroupListener(new ImServerGroupListener());
        serverTioConfig.setName(ImConfig.PROTOCOL_NAME);
        serverTioConfig.setTioServerListener(ImTioServerListener.me);
        serverTioConfig.ipStats.addDurations(ImConfig.IpStatDuration.IP_STAT_DURATIONS);
        serverTioConfig.setHeartbeatTimeout(CourierConfig.get().getSocketHeartbeat());
    }

    public static void start() throws Exception {
        ImServerWebSocketStart appStarter = new ImServerWebSocketStart(CourierConfig.get().getSocketPort(), WsMsgHandler.me);

        ImConfig imServerConfig = new ImConfig();
        imServerConfig.setImUserListener(new ImServerUserListener());
        imServerConfig.setTioConfig(appStarter.serverTioConfig);
        imServerConfig.setImUserPropListener(new ImServerUserPropListener());
        imServerConfig.setImGroupPropListener(new ImServerGroupPropListener());

        appStarter.wsServerStarter.start();
    }
}

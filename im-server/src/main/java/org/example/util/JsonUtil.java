package org.example.util;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import org.tio.core.intf.Packet;
import org.tio.websocket.common.WsPacket;

import java.nio.charset.StandardCharsets;

public class JsonUtil {


    public static <T> T format(Packet packet, Class<T> clazz) {
        return JSON.parseObject(packetBody(packet), clazz);
    }

    public static String packetBody(Packet packet) {
        WsPacket wsPacket = (WsPacket) packet;
        return StrUtil.str(wsPacket.getBody(), StandardCharsets.UTF_8);
    }
}

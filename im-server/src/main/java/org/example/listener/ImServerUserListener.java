package org.example.listener;

import org.example.component.LoginInfo;
import org.example.packets.bean.User;
import org.example.service.UserService;
import org.tio.core.ChannelContext;

import static org.example.component.AbstractStorage.setValue;

public class ImServerUserListener implements ImUserListener {

    private final UserService userService;

    public ImServerUserListener() {
        this.userService = new UserService();
    }

    @Override
    public void onAfterBind(ChannelContext channelContext, User user) {
        System.out.println("用户绑定成功");
        // 用户绑定成功后的事件
        // 当前的上下文信息
        setValue(new LoginInfo().setChannelContext(channelContext));

    }

    @Override
    public void onAfterUnbind(ChannelContext channelContext, User user) {

    }

    @Override
    public void onAfterChange(User user) {
        users.put(user.getId(), user);
        userService.updateById(user);
    }

    @Override
    public User onGetUser(String userId) {
        User user = users.get(userId);
        if (null == user) {
            User userInfo = userService.getUserInfo(userId);
            users.put(userId, userInfo);
            return userInfo;
        }
        return user;
    }
}

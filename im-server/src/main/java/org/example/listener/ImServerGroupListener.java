package org.example.listener;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import lombok.extern.slf4j.Slf4j;
import org.example.component.LoginInfo;
import org.example.component.UserInfo;
import org.example.config.ImConfig;
import org.example.enums.RoomRoleEnum;
import org.example.packets.bean.Group;
import org.example.packets.bean.User;
import org.example.packets.bean.UserGroup;
import org.example.packets.handler.user.GroupUser;
import org.example.service.GroupService;
import org.example.service.UserGroupService;
import org.example.util.ChannelContextUtil;
import org.tio.core.ChannelContext;
import org.tio.core.Tio;

import java.util.List;
import java.util.stream.Collectors;

import static org.example.component.AbstractStorage.setValue;

@Slf4j
public class ImServerGroupListener implements ImGroupListener {

    public final UserGroupService userGroupService = new UserGroupService();
    public final GroupService groupService = new GroupService();


    @Override
    public void onAfterBind(ChannelContext channelContext, String group) {
        LoginInfo loginInfo = new LoginInfo().setChannelContext(channelContext);
        if (loginInfo.getGroupsMap().get(group) != null) {
            return;
        }
        // TODO  用户绑定信息
        List<UserGroup> groupUsers = userGroupService.getGroupUsers(group);
        Group groupInfo = userGroupService.getUserGroup(group, loginInfo.getUser().getId(), groupUsers);

        List<GroupUser> users = groupInfo.getUsers().stream().peek(u -> {
            User userInfo = ImConfig.userListener().onGetUser(u.getId());
            BeanUtil.copyProperties(userInfo, u, "role", "black");
            // 如果是群组, 并且ID不等于当前用户, 并且房间名为空 说明就是对方且没有备注, 那么改名字咯
            if (groupInfo.getIsFriend() && StrUtil.isBlank(groupInfo.getRoomName()) && !userInfo.getId().equals(loginInfo.getUser().getId())) {
                groupInfo.setFriendId(userInfo.getId());
                groupInfo.setAvatar(userInfo.getAvatar());
                groupInfo.setRoomName(userInfo.getUsername());
            }
        }).collect(Collectors.toList());
        groupInfo.setUnreadCount(0);
        groupInfo.setUsers(users);
        loginInfo.putGroup(groupInfo);

        if (channelContext.userid.equals(UserInfo.id())) {
            setValue(loginInfo);
        }

    }

    @Override
    public void onAfterUnbind(ChannelContext channelContext, String group) {

    }

    @Override
    public void onRemoveUser(String roomId, String userId) {
        Tio.unbindGroup(ImConfig.get().tioConfig, userId, roomId);

        // 将当前所有的连接端都解除掉
        List<ChannelContext> channelContexts = ChannelContextUtil.contextList(userId);
        for (ChannelContext channelContext : channelContexts) {
            LoginInfo loginInfo = new LoginInfo().setChannelContext(channelContext);
            loginInfo.removeGroup(roomId);
        }

    }

    public void onDisband(String roomId) {
        List<ChannelContext> groupChannels = ChannelContextUtil.groupUsers(roomId);
        for (ChannelContext channelContext : groupChannels) {
            Tio.unbindGroup(roomId, channelContext);
            LoginInfo loginInfo = new LoginInfo().setChannelContext(channelContext);
            loginInfo.removeGroup(roomId);
        }
    }

    public void onRoleChange(String roomId, String userId, RoomRoleEnum role) {
        List<ChannelContext> channelContexts = ChannelContextUtil.groupUsers(roomId);
        for (ChannelContext channelContext : channelContexts) {
            LoginInfo loginInfo = new LoginInfo().setChannelContext(channelContext);
            Group group = loginInfo.getGroupsMap().get(roomId);
            for (GroupUser user : group.getUsers()) {
                if (user.getId().equals(userId)) {
                    user.setRole(role);
                    break;
                }
            }
            loginInfo.putGroup(group);
        }
    }

    @Override
    public void onBlack(String roomId, String userId, Boolean black) {
        List<ChannelContext> channelContexts = ChannelContextUtil.groupUsers(roomId);
        for (ChannelContext channelContext : channelContexts) {
            LoginInfo loginInfo = new LoginInfo().setChannelContext(channelContext);
            Group group = loginInfo.getGroupsMap().get(roomId);

            // 如果是自己的配置
            if (channelContext.userid.equals(userId)) {
                group.setBlack(black);
            }
            for (GroupUser user : group.getUsers()) {
                if (user.getId().equals(userId)) {
                    user.setBlack(black);
                }
            }

            loginInfo.putGroup(group);
        }
    }

    @Override
    public void onMoveTop(String roomId, String userId, Long index) {
        List<ChannelContext> channelContexts = ChannelContextUtil.contextList(userId);
        for (ChannelContext channelContext : channelContexts) {
            LoginInfo loginInfo = new LoginInfo().setChannelContext(channelContext);
            Group group = loginInfo.getGroupsMap().get(roomId);
            group.setIndex(index);
            loginInfo.putGroup(group);
        }
    }


}
